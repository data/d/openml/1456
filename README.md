# OpenML dataset: appendicitis

https://www.openml.org/d/1456

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: S. M. Weiss,C. A. Kulikowski  
**Source**: KEEL
**Please cite**:   

A copy of the data set proposed in: S. M. Weiss, and C. A. Kulikowski,  Computer Systems That Learn (1991).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1456) of an [OpenML dataset](https://www.openml.org/d/1456). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1456/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1456/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1456/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

